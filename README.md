# Biblioteca para generación de grafos aleatorios y algoritmos sobre grafos.


## Algoritmos para la generación de grafos aleatorios
* Gilbert
    - Crea *m* aristas crea *n* vertices y poner una arista entre cada par independiente y uniformemente con probabilidad *p*

* Geografico Simple
    - Coloca *n* vértices en un rectangulo unitario con coordenadas uniformes (o normales) y coloca una arista entre cada par que queda en distancia *r* o menor.

* Barabasi
    - Coloca *n* vértices uno por uno, asignando a cada uno *d* aristas a vértices distintos de tal manera que laprobabilidad de que el vértice nuevo se conecte a un vértice existente *v* es proporcional a la cantidad de aristas que *v* tiene actualmente - los primeros dvértices se conecta todos a todos.

## Algoritmos sobre grafos

* [BFS](https://es.wikipedia.org/wiki/B%C3%BAsqueda_en_anchura)
* [DFS](https://es.wikipedia.org/wiki/B%C3%BAsqueda_en_profundidad) <- Iterativo
* [DFS](https://es.wikipedia.org/wiki/B%C3%BAsqueda_en_profundidad) <- Recursivo
* [Prim](https://es.wikipedia.org/wiki/Algoritmo_de_Prim)
* [Kruskal](https://es.wikipedia.org/wiki/Algoritmo_de_Kruskal) <- Directo
* [Kruskal](https://es.wikipedia.org/wiki/Algoritmo_de_Kruskal) <- Inverso
* [Dijkstra](https://es.wikipedia.org/wiki/Algoritmo_de_Dijkstra)


## Construido con 🛠

* Java

## Requisitos

Para que se pueda generar imagenes de los grafos en formato .png se necesita la libreria

```
Graphviz
```

## Estado
#### Finalizado sin continuidad

## Imagenes de grafos generados
![alt text](./outputs/geografico/dijkstra/img/geografico_dijkstra_30.png)
![alt text](./outputs/barabasi/bfs_dfs/img/barabasi_bfs_30.png)
