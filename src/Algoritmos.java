import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;

/**
 * Class Algortimos
 *
 * Su funcion es proveer metodos que ejecuten algortimos sobre grafos.
 * La Clase se construye con un objeto de tipo Grafo y cada metodo crea
 * un nuevo objeto Grafo.
 * Cabe resaltar que los algoritmos actuan sobre el conjunto, de nodos
 * conectados, en donde se encunentre el nodo inicial de cada algoritmo.
 * Esto tiene inpacto en los siguientes escenarios:
 *
 * - El grafo es un solo conjunto conectado (un grafo conexo) --Caso ideal--
 *      Entonces los algoritmos operaran sobre todo el grafo
 *
 * - El grafo tiene más de un conjunto de nodos conectados -- Caso posible --
 *      Entonces los algoritmos actuaran sobre el conjunto que contenga el nodo inicial,
 *      y los demás nodos apareceran el el .dot y .png como vertices independites (sin aristas)
 *
 * - El grafo no tiene conecciones -- pero caso --
 *      Entonces los algoritmos no modifican nada.
 */
public class Algoritmos {
    Grafo g;

    public Algoritmos(Grafo g){
        this.g = g;
    }

    /**
     * Algoritmo BFS (Breadth First Search)
     *
     * @param s nodo inicial
     * @return Arbol bfs
     */
    public Grafo bfs(Nodo s) {
        Grafo grafo_bfs = new Grafo();
        grafo_bfs.setNodos(this.g.getNodos());
        LinkedList<Nodo> colaNodos = new LinkedList<Nodo>(); //cola para guardar el nodo 'actual' del recorrido
        List<Nodo> visitados = new ArrayList<Nodo>(); //Lista de nodos visitados
        colaNodos.add(s); // se agrega a la cola
        visitados.add(s); // se vista
        Nodo actual;
        while (!colaNodos.isEmpty()) {
            actual = colaNodos.poll(); // Nodo actual
            // itera sobre las aristas adyacentes a 'actual'
            // Ver la descripcion de getAristas para enterder como optiene las adyacentes.
            for (Arista w : this.g.getAristas(actual)) {
                /*Si el nodo actual es el nodo destino de la arista y el grafo es no dirigido
                entonces, el nodo adyacente es el nodo origen, sino el adyacente es el destino de la arista*/
                Nodo adyacente = actual.equals(w.getDestino()) && !this.g.isDirigido()
                                 ? w.getOrigen(): w.getDestino();
                if (!visitados.contains(adyacente)) {
                    // El nodo adyacente no ha sido visitado
                    grafo_bfs.addArista(w.getId(), w); // se agrega la arista
                    visitados.add(adyacente); // se visita adyacente
                    colaNodos.add(adyacente); // se agrega a la cola
                }
            }
        }
        return grafo_bfs;
    }

    /**
     * Algoritmo DFS (Breadth First Search) iterativo
     *
     * @param s nodo inicial
     * @return Arbol dfs iterativo
     */
    public Grafo dfs_i(Nodo s) {
        Grafo grafo_dfs_i = new Grafo();
        grafo_dfs_i.setNodos(this.g.getNodos());
        Stack<Nodo> pila = new Stack<Nodo>(); // Pila para guardar los nodos
        List<Nodo> visitados = new ArrayList<Nodo>();
        pila.push(s);
        visitados.add(s);
        Nodo actual;
        while (!pila.isEmpty()) {
            actual = pila.pop(); // se optiene el nodo actual de la pila
            List<Arista> l = this.g.getAristas(actual); //Lista de aristas conectadas con el nodo actual
            for (Arista w : l) {
                /*Si el nodo actual es el nodo destino de la arista y el grafo es no dirigido
                entonces, el nodo adyacente es el nodo origen, sino el adyacente es el destino de la arista*/
                Nodo adyacente = actual.equals(w.getDestino()) && !this.g.isDirigido()
                                 ? w.getOrigen(): w.getDestino();
                if (!visitados.contains(adyacente)) {
                    grafo_dfs_i.addArista(w.getId(), w);
                    visitados.add(adyacente);
                    pila.push(adyacente);
                }
            }
        }
        return grafo_dfs_i;
    }

    /**
     * Algoritmo DFS (Depth First Search)
     *
     * @param actual Nodo actual del recorrido
     * @param visitados Lista con los nodos ya visitados
     * @param grafo Grafo en donde se evalua el algoritmo
     */
    public void dfs_recursion(Nodo actual, List<Nodo> visitados, Grafo grafo){
        visitados.add(actual);
        for (Arista v: this.g.getAristas(actual)) {
            Nodo adyacente = actual.equals(v.getDestino()) && !this.g.isDirigido()
                                 ? v.getOrigen(): v.getDestino();
            if (!visitados.contains(adyacente)) {
                grafo.addArista(v.getId(), v);
                visitados.add(adyacente);
                dfs_recursion(adyacente, visitados, grafo); // Llamada recursiva
            }
        }
    }

    /**
     *
     * @param s Nodo inicial
     * @return Arbol dfs recursivo
     */
    public Grafo dfs_r(Nodo s) {
        Grafo grafo_dfs_r = new Grafo();
        grafo_dfs_r.setNodos(this.g.getNodos());
        List<Nodo> visitados = new ArrayList<Nodo>();
        dfs_recursion(s, visitados, grafo_dfs_r);
        return grafo_dfs_r;
    }

    /**
     * Ejecuta el algoritmo de prim en un grafo conexo, no dirigido
     * y cuyas aristas están etiquetadas, para encontrar un árbol recubridor mínimo.
     *
     * El coste total del arbol lo guarda en un archivo .txt
     *
     * @param dir Direccion donde guardar el .txt con el coste total del arbol
     * @param nombre Nombre del grafo
     * @return Arbol de expansión mínima
     */
    public Grafo prim(String dir, String nombre) {
        Grafo grafo_prim = new Grafo();
        grafo_prim.setNodos(this.g.getNodos());
        Set<Arista> adyacentes = new HashSet<Arista>(); //conjunto para almacenar las Aristas adyacentes
        List<Nodo> visitados = new ArrayList<Nodo>(); // Lista de nodos ya visitados
        List<Nodo> list_nodos = new ArrayList<Nodo>(); //Lista para guardar los nodos del grafo
        int val = (int) (Math.random()*this.g.getNodos().size()-1);//Key, aleatoria, del nodo inicial
        Nodo inicial = this.g.getNodos().get(val); //nodo inicial (aleatorio)
        Arista n_aux = new Arista(); // Se almacenara la Arista con el coste minimo
        float min = 0;// Se almacenara el coste minimo de las aristas adyacentes
        float suma = 0;
        // Se pasan todos los Nodos del grafo a la Lista
        for (Nodo v : this.g.getNodos().values())
            list_nodos.add(v);
        adyacentes.addAll(this.g.getAristas(inicial)); // Obtenemos las aristas adyacentes de 'inicial' y las agregamos al conjunto
        visitados.add(inicial); //agregamos el nodo incial a visitados
        //condicion para verificar que el nodo inicial esta conectado al grafo
        if (adyacentes.isEmpty())
            System.out.println("PRIM: El nodo inicial: "+inicial.getLabel()+" No tiene aristas adyacentes");
        /* la condicion para que se detenga el ciclo es que se hayan visitado todos los nodos (!list_nodos.isEmpty())
            o que ya no existan aristas adyacentes por recorrer, esto porque puede haber nodos que no esten conectados*/
        while (!list_nodos.isEmpty() && !adyacentes.isEmpty()) {
            // Recorremos cada arista adyacente para encontrar la minima en coste
            Arista arista_aux = (Arista) adyacentes.toArray()[0]; //La primera Arista de la lista de aristas adyacentes
            min = arista_aux.getPeso(); // el valor minimo de las aristas adyacentes
            for (Arista a : adyacentes) {
                // if que compara el min actual con el valor de cada adyacente
                if (a.getPeso() <= min) {
                    min = a.getPeso();
                    n_aux = a; // Arista que tiene el coste minimo
                }
            }
            /*Si visitados ya contiene el nodo destino de la arista de menor coste,
            y el grafo es no dirigido, entonces, el nodo adyacente es el nodo origen,
            sino el adyacente es el nodo destino de la arista*/
            Nodo adyacente = visitados.contains(n_aux.getDestino()) && !this.g.isDirigido()
                                 ? n_aux.getOrigen(): n_aux.getDestino();
            if (!visitados.contains(adyacente)) {
                //Si el nodo adyacente no esta en visitados
                grafo_prim.addArista(n_aux.getId(), n_aux); // se agrega la arista
                visitados.add(adyacente); //agregamos el nodo a visitados
                list_nodos.remove(adyacente); // eliminamos el nodo de list_nodos
                adyacentes.addAll(this.g.getAristas(adyacente)); // Obtenemos las aristas adyacentes del nodo y las agregamos al conjunto
                suma = suma + min;
            }
            // siempre se elimina la arista de el conjunto adyacentes
            adyacentes.remove(n_aux);
        }
        //Se escribe el archivo con el coste total mínimo del arbol
        write(dir+"/img/TOTAL_"+nombre+"_prim.txt","PESO DEL ARBOL PRIM DE EXPANCIÓN MINIMA :\n"+Float.toString(suma),false);
        return grafo_prim;
    }

    /**
     * Kruskal directo.
     *
     * El algoritmo de Kruskal es un algoritmo de la teoría de grafos para encontrar
     * un árbol recubridor mínimo en un grafo conexo y ponderado. Es decir, busca un
     * subconjunto de aristas que, formando un árbol, incluyen todos los vértices y
     * donde el valor de la suma de todas las aristas del árbol es el mínimo.
     *
     * El coste total del arbol lo guarda en un archivo .txt
     *
     * @param dir Direccion donde guardar el .txt con el coste total del arbol
     * @param nombre Nombre del grafo
     * @return Arbol de expansión mínima
     */
    public Grafo kruskal_d(String dir, String nombre) {
        Grafo grafo_kruskal = new Grafo();
        grafo_kruskal.setNodos(this.g.getNodos());
        LinkedList<Arista> cola_aristas = new LinkedList<Arista>(); // cola de aristas
        /* array donde se guardaran los padres de cada nodo. El indice es el id del nodo
           y el valor del indice es el padre del nodo. Ejemplo:
                index (nodo hijo) ->   0 1 2
                valor (nodo padre)-> [-1,2,0]
           El -1 indica que no tiene padre por lo tanto es un nodo raiz. El recorrido es
           recursivo y nos sirve para identificar si dos nodos pertenecen a un mismo
           conjunto de nodos conectados */
        int roots [] = new int[this.g.getNodos().size()];
        Arista less;
        float suma = 0;
        int x_origen, y_origen;
        this.g.getNodos().forEach((key, tab)->roots[key]=-1); // se inicializa roots con -1
        this.g.getAristas().forEach((key, tab)->cola_aristas.add(tab)); // Se pasan todas las aristas a la cola
        //Lambda expression para ordenar las aristas por peso, ascendentemente
        cola_aristas.sort((Arista s1, Arista s2)->Float.compare(s1.getPeso(), s2.getPeso()));
        try {
            less = cola_aristas.poll(); // se optiene la arista con menor peso
            suma = less.getPeso();
            grafo_kruskal.addArista(less.getId(), less); // se agrega la arista
            x_origen = less.getOrigen().getId(); // se optiene el id del nodo origen
            y_origen = less.getDestino().getId(); // se optiene el id del nodo destino
            union(roots,x_origen,y_origen); // se unen origen y destino, en un mismo conjunto
        } catch (NullPointerException e) {
            System.out.println("KRUSKAL: El grafo no tiene Aristas");
        }
        
        while (!cola_aristas.isEmpty()) {
            less = cola_aristas.poll(); // se optiene la arista siguiente con menor peso
            x_origen = less.getOrigen().getId(); // se optiene el id del nodo origen
            y_origen = less.getDestino().getId(); // se optiene el id del nodo destino
            /* Se busca el padre de cada nodo. Si el padre es el mismo para los dos, entonces,
               los nodos ya pertencen a un mismo conjunto, sino, los nodos estan en conjuntos
               diferentes, por lo cual los puedo unir*/
            if (find(roots, x_origen) != find(roots, y_origen)) {
                grafo_kruskal.addArista(less.getId(), less); // add arista
                suma = suma + less.getPeso();
                union(roots,x_origen,y_origen);// se unen los nodos
            }
        }
        //Se escribe el archivo con el coste total mínimo del arbol
        write(dir+"/img/TOTAL_"+nombre+"_kruskal_d.txt","PESO DEL ARBOL KRUSKAL DIRECTO DE EXPANCIÓN MINIMA :\n"+Float.toString(suma),false);
        return grafo_kruskal;
    }

    /**
     * Kruskal inverso
     *
     * Tiene la misma función que el Kruskal directo, pero con un mecanismo distinto.
     * En vez de optener la arista de menor peso y ver si los nodos se encuentran ya en
     * un mismo conjunto y de acuerdo a estó se decide si se conectan o no.
     * El kruskal inverso, comienza con un grafo ya conectado y se elije la arista
     * con mayor peso y si al eliminarla no se desconecta el grafo, entonces la
     * elimina y así con todas las aristas.
     *
     * El coste total del arbol lo guarda en un archivo .txt
     *
     * @param dir Direccion donde guardar el .txt con el coste total del arbol
     * @param nombre Nombre del grafo
     * @return Arbol de expansión mínima
     */
    public Grafo kruskal_i(String dir, String nombre) {
        Grafo grafo_kruskal = new Grafo();
        grafo_kruskal.setNodos(this.g.getNodos());
        LinkedList<Arista> cola_aristas = new LinkedList<Arista>();
        float suma = 0;
        // Se pasan todos las aristas del grafo a la Lista
        this.g.getAristas().forEach((key, tab)->cola_aristas.add(tab));
        //Lambda expression para ordenar las aristas por peso, descendentemente
        cola_aristas.sort((Arista s1, Arista s2)->Float.compare(s2.getPeso(), s1.getPeso()));
        for (Arista e : cola_aristas) { // para cada arista
            this.g.deleteA(e); // se elimina del grafo
            //se verifica que el grafo aún siga conectado
            if (!isStillConnected(e,grafo_kruskal)) {
                grafo_kruskal.addArista(e.getId(), e); //add arista
                suma = suma + e.getPeso();
            }
        }
        // Se vuelven a poner todas las aristas al grafo original
        cola_aristas.forEach(e->this.g.addArista(e.getId(),e));
        write(dir+"/img/TOTAL_"+nombre+"_kruskal_i.txt","PESO DEL ARBOL KRUSKAL INVERSO DE EXPANCIÓN MINIMA :\n "+Float.toString(suma),false);
        return grafo_kruskal;
    }

    /**
     * Dijkstra
     *
     * El algoritmo de Dijkstra, es un algoritmo para la determinación del camino más corto,
     * dado un vértice origen, hacia el resto de los vértices en un grafo que tiene pesos
     * en cada arista.
     *
     * @param s Nodo inicial
     * @param d Nodo a donde se calcula el camino desde el origen
     * @param dir Direccion donde guardar el .txt con el coste del camino de s a d
     * @param nombre Nombre del grafo
     * @return Arbol de caminos mínimos
     */
    public Grafo dijkstra(Nodo s, Nodo d, String dir, String nombre){
        Grafo grafo_dijkstra = new Grafo();
        grafo_dijkstra.setNodos(this.g.getNodos());
        PriorityQueue<Nodo> pQueue = new PriorityQueue<Nodo>(); // cola de prioridad
        //array de nodos visitados
        boolean [] visitados = new boolean[this.g.getNodos().size()];
        //array de distancia de cada nodo
        float [] distancia = new float[this.g.getNodos().size()];
        //array de nodo previo
        int previo [] = new int[this.g.getNodos().size()];
        this.g.getNodos().forEach((key, tab)->{
            //Se inicializan los arreglos
            previo[key]=-1;
            distancia[key]=10000;
            visitados[key] = false;});
        s.setPeso(0);
        pQueue.add(s);
        distancia[s.getId()] = 0;
        while(!pQueue.isEmpty()){
            Nodo u = pQueue.poll();
            if (visitados[u.getId()])
                continue;
            visitados[u.getId()] = true;
            for (Arista v : this.g.getAristas(u)) {
                Nodo adyacente = u.equals(v.getDestino()) && !this.g.isDirigido()
                                 ? v.getOrigen(): v.getDestino();
                float peso = v.getPeso();
                if (!visitados[adyacente.getId()])
                    marcar(u.getId(), adyacente, peso, distancia, previo, pQueue);
            }
        }

        /** Al termino del algoritmo el arbol se encuentra en el arreglo previo,
         * pero es un arreglo de enteros, que contiene solo los id de los nodos.
         * Se necesita crear un objeto Grafo partiendo de los id de los nodos
        */
        float suma[] = new float[1];
        for (int i = 0; i < previo.length; i++) {
            if (previo[i] != -1){
                Nodo origen = this.g.getNodos().get(previo[i]);
                Nodo destino = this.g.getNodos().get(i);
                Arista a = new Arista(i,origen,destino,this.g.getPeso(origen,destino));
                grafo_dijkstra.addArista(i, a);
            }
        }
        /* ACLARACION: No pudé hacer que se visualizara la distancia del camino minimo
        como se da la indicacion en el ejercicio, al menos no en la imagen .png. Probalemente
        si se habre con algun programa el .dot si se vea.
        Aun así se detalla el camino de origen a destino en un archivo*/
        String desc = "CAMINO DEL NODO: "+s.getLabel()+" AL NODO: "+d.getLabel()+
        "\n";
        write(dir+"/img/TOTAL_"+nombre+"_dijkstra.txt",desc,true);
        printCamino(d.getId(), previo, dir+"/img/TOTAL_"+nombre+"_dijkstra.txt",suma,grafo_dijkstra);
        write(dir+"/img/TOTAL_"+nombre+"_dijkstra.txt","Distancia total del recorrido = "+Float.toString(suma[0])+"\n",true);
        return grafo_dijkstra;
    }

    /**
     * Se utiliza para marcar y actulizar los valores de distancia,
     * previo y la cola de prioridad de acuerdo a la condicion:
     * si distancia actual + peso es menor a distancia del nodo adyacente
     *
     * @param actual Id del nodo actual
     * @param adyacente Nodo adyacente
     * @param peso peso de la arista
     * @param distancia [] de distancias
     * @param previo [] de previos
     * @param pQueue cola de prioridad
     */
    private void marcar(int actual, Nodo adyacente, float peso, float[] distancia, int[] previo, PriorityQueue<Nodo> pQueue){
        if ( (distancia[actual]+peso) < distancia[adyacente.getId()] ) {
            distancia[adyacente.getId()] = distancia[actual]+peso;
            previo[adyacente.getId()] = actual;
            adyacente.setPeso(distancia[actual]+peso);
            pQueue.add(adyacente);
        }
    }

    /**
     * Dependiente del algoritmo de DIJKSTRA.
     *
     * Imprime el camino minimo del nodo 'inicio' a 'destino'.
     * Realiza un recorrido recursivo sobre el array previo
     *
     * NOTA: no esta en uso porque no es un requisito, pero lo dejo
     *       por si en algun momento lo utilizo
     *
     * @param destino id del nodo destino
     * @param previo [] de previos
     */
    private void printCamino(int destino, int[] previo, String dir, float[] suma, Grafo g){
        Nodo o = new Nodo();
        Nodo d = new Nodo();
        if( previo[ destino ] != -1 ){    //si aun poseo un vertice previo
            o = this.g.getNodos().get(previo[destino]);
            d = this.g.getNodos().get(destino);
            suma[0] = suma[0] + this.g.getPeso(o,d);
            g.getAristas().get(destino).setColor("red");
            printCamino( previo[ destino ], previo, dir, suma, g);  //recursivamente sigo explorando
            write(dir,"Ir de: "+ previo[ destino ]+ " a: " +destino+" con peso "+this.g.getPeso(o,d), true);
        }
    }

    /**
     * Dependiente del algoritmo de Kruskal inverso
     *
     * Basicamente es el algoritmo bfs pero modificado para que regrese true si el grafo
     * está conectado y false si no lo está
     *
     * @param e Arista inicial
     * @param g Grafo
     * @return
     */
    private Boolean isStillConnected(Arista e, Grafo g) {
        boolean respuesta = false;
        LinkedList<Nodo> colaNodos = new LinkedList<Nodo>(); //cola para guardar el nodo 'actual' del recorrido
        List<Nodo> visitados = new ArrayList<Nodo>();
        colaNodos.add(e.getOrigen());
        visitados.add(e.getOrigen());
        Nodo actual;
        while (!colaNodos.isEmpty()) {
            actual = colaNodos.poll();
            for (Arista w : this.g.getAristas(actual)) { // itera sobre las aristas adyacentes a 'actual'
                /*Si el nodo actual es el nodo destino de la arista y el grafo es no dirigido
                entonces, el nodo adyacente es el nodo origen, sino el adyacente es el destino de la arista*/
                Nodo adyacente = actual.equals(w.getDestino()) && !this.g.isDirigido()
                                 ? w.getOrigen(): w.getDestino();
                if (!visitados.contains(adyacente)) {
                    visitados.add(adyacente);
                    colaNodos.add(adyacente);
                }
            }
        }
        // si vistidos contiene el destino entonces el grafo está conectado
        if (visitados.contains(e.getDestino()))
            respuesta = true;
        return respuesta;
    }

    /**
     * Dependecia de Kruskal directo
     *
     * Busca el nodo padre de un nodo, lo hace mediante los id de los nodos
     * de manera recursiva.
     *
     * @param roots [] De nodos padre - hijo (hijo -> index, padre -> valor)
     * @param i id del nodo a buscar su padre
     * @return id del nodo padre
     */
    private int find(int[] roots, int i){
        if (roots[i] == -1)
            return i;
        return find(roots, roots[i]);
    }

    /**
     * Dependecia de Kruskal directo
     *
     * Une en un mismo conjunto de nodos conectados a un par de nodos, mediante
     * su id.
     *
     * @param roots [] De nodos padre - hijo (hijo -> index, padre -> valor)
     * @param x id del nodo origen
     * @param y id del nodo destino
     */
    private void union(int[] roots, int x, int y){
        int xset = find(roots, x);
        int yset = find(roots, y);
        roots[xset] = yset;
    }

    /**
     * Dependencia: prim, kruskal directo, kruskal inverso.
     *
     * Escreibe un archivo .txt con el coste minimo de un arbol de expancion
     *
     * @param direccionDot Dirección donde se gurdara el archivo
     * @param nombre nombre del arbol
     * @param suma suma del coste
     */
    private void write(String direccionDot, String texto, boolean append) {
        try(BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(direccionDot,append))) ){
            out.write(texto);
            out.newLine();
            out.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}