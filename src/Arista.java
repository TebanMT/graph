
/**
 * Class Arista
 *
 * Contiene las propiedades de una arista.
*/
public class Arista {
    private int id;
    private Nodo origen;
    private Nodo destino;
    private float peso;
    private String color;

    public Arista(int id, Nodo origen, Nodo destino, float peso) {
        this.id = id;
        this.origen = origen;
        this.destino = destino;
        this.peso = peso;
        this.color = "mediumturquoise";
    }

    /**Sobrecarga del constructor, esto nos ayudara a declarar aristas 'vacias'
     * que pueden ser de utilizad cuando aun no sabmos que valores tomaran.
    */
    public Arista(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Nodo getOrigen() {
        return origen;
    }

    public void setOrigen(Nodo origen) {
        this.origen = origen;
    }

    public Nodo getDestino() {
        return destino;
    }

    public void setDestino(Nodo destino) {
        this.destino = destino;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}