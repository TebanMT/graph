
public class Main {
    public static void main(String[] args) {

        boolean dirigido = false;
        boolean ciclos = false;
        int amout_v = 30; // amount de nodos

        // Grafo Erdos
        Grafo g_e = Grafo.erdosGraph(amout_v, 50, dirigido, ciclos);
        Algoritmos algoritmos = new Algoritmos(g_e);
        g_e.randomEdgeValues(1, 10, g_e);

        g_e.write("./graph2/outputs/erdos/dot/erdos_"+amout_v+".dot",g_e, dirigido);
        g_e.dibujar("./graph2/outputs/erdos/dot/erdos_"+amout_v+".dot",
        "./graph2/outputs/erdos/img/erdos_"+amout_v+".png");

        Grafo bfs = algoritmos.bfs(g_e.getNodos().get(0));
        bfs.write("./graph2/outputs/erdos/bfs_dfs/dot/erdos_bfs_"+amout_v+".dot",bfs, dirigido);
        bfs.dibujar("./graph2/outputs/erdos/bfs_dfs/dot/erdos_bfs_"+amout_v+".dot",
        "./graph2/outputs/erdos/bfs_dfs/img/erdos_bfs_"+amout_v+".png");

        Grafo dfs_i = algoritmos.dfs_i(g_e.getNodos().get(0));
        dfs_i.write("./graph2/outputs/erdos/bfs_dfs/dot/erdos_dfs_i_"+amout_v+".dot",dfs_i, dirigido);
        dfs_i.dibujar("./graph2/outputs/erdos/bfs_dfs/dot/erdos_dfs_i_"+amout_v+".dot",
        "./graph2/outputs/erdos/bfs_dfs/img/erdos_dfs_i_"+amout_v+".png");

        Grafo dfs_r = algoritmos.dfs_r(g_e.getNodos().get(0));
        dfs_r.write("./graph2/outputs/erdos/bfs_dfs/dot/erdos_dfs_r_"+amout_v+".dot",dfs_r, dirigido);
        dfs_r.dibujar("./graph2/outputs/erdos/bfs_dfs/dot/erdos_dfs_r_"+amout_v+".dot",
        "./graph2/outputs/erdos/bfs_dfs/img/erdos_dfs_r_"+amout_v+".png");

        Grafo prim = algoritmos.prim("./graph2/outputs/erdos/prim_kruskal","ERDOS_"+amout_v);
        prim.write("./graph2/outputs/erdos/prim_kruskal/dot/erdos_prim_"+amout_v+".dot",prim, dirigido);
        prim.dibujar("./graph2/outputs/erdos/prim_kruskal/dot/erdos_prim_"+amout_v+".dot",
        "./graph2/outputs/erdos/prim_kruskal/img/erdos_prim_"+amout_v+".png");

        Grafo kruskal_d = algoritmos.kruskal_d("./graph2/outputs/erdos/prim_kruskal","ERDOS_"+amout_v);
        kruskal_d.write("./graph2/outputs/erdos/prim_kruskal/dot/erdos_kruskal_d_"+amout_v+".dot",kruskal_d, dirigido);
        kruskal_d.dibujar("./graph2/outputs/erdos/prim_kruskal/dot/erdos_kruskal_d_"+amout_v+".dot",
        "./graph2/outputs/erdos/prim_kruskal/img/erdos_kruskal_d_"+amout_v+".png");

        Grafo kruskal_i = algoritmos.kruskal_i("./graph2/outputs/erdos/prim_kruskal","ERDOS_"+amout_v);
        kruskal_i.write("./graph2/outputs/erdos/prim_kruskal/dot/erdos_kruskal_i_"+amout_v+".dot",kruskal_i, dirigido);
        kruskal_i.dibujar("./graph2/outputs/erdos/prim_kruskal/dot/erdos_kruskal_i_"+amout_v+".dot",
        "./graph2/outputs/erdos/prim_kruskal/img/erdos_kruskal_i_"+amout_v+".png");

        Grafo dijkstra = algoritmos.dijkstra(g_e.getNodos().get(0), g_e.getNodos().get((int)(Math.random()*(amout_v)-1)), "./graph2/outputs/erdos/dijkstra","ERDOS_"+amout_v);
        dijkstra.write("./graph2/outputs/erdos/dijkstra/dot/erdos_dijkstra_"+amout_v+".dot",dijkstra, dirigido);
        dijkstra.dibujar("./graph2/outputs/erdos/dijkstra/dot/erdos_dijkstra_"+amout_v+".dot",
        "./graph2/outputs/erdos/dijkstra/img/erdos_dijkstra_"+amout_v+".png");

        // Grafo Gilbert
        Grafo g_g = Grafo.gilbertGraph(amout_v, 60, 0.7, dirigido, ciclos);
        Algoritmos algoritmos_g_g = new Algoritmos(g_g);
        g_g.randomEdgeValues(1, 10, g_g);

        g_g.write("./graph2/outputs/gilbert/dot/gilbert_"+amout_v+".dot",g_g, dirigido);
        g_g.dibujar("./graph2/outputs/gilbert/dot/gilbert_"+amout_v+".dot",
        "./graph2/outputs/gilbert/img/gilbert_"+amout_v+".png");

        Grafo bfs_g = algoritmos_g_g.bfs(g_g.getNodos().get(0));
        bfs_g.write("./graph2/outputs/gilbert/bfs_dfs/dot/gilbert_bfs_"+amout_v+".dot",bfs_g, dirigido);
        bfs_g.dibujar("./graph2/outputs/gilbert/bfs_dfs/dot/gilbert_bfs_"+amout_v+".dot",
        "./graph2/outputs/gilbert/bfs_dfs/img/gilbert_bfs_"+amout_v+".png");

        Grafo dfs_i_g = algoritmos_g_g.dfs_i(g_g.getNodos().get(0));
        dfs_i_g.write("./graph2/outputs/gilbert/bfs_dfs/dot/gilbert_dfs_i_"+amout_v+".dot",dfs_i_g, dirigido);
        dfs_i_g.dibujar("./graph2/outputs/gilbert/bfs_dfs/dot/gilbert_dfs_i_"+amout_v+".dot",
        "./graph2/outputs/gilbert/bfs_dfs/img/gilbert_dfs_i_"+amout_v+".png");

        Grafo dfs_r_g = algoritmos_g_g.dfs_r(g_g.getNodos().get(0));
        dfs_r_g.write("./graph2/outputs/gilbert/bfs_dfs/dot/gilbert_dfs_r_"+amout_v+".dot",dfs_r_g, dirigido);
        dfs_r_g.dibujar("./graph2/outputs/gilbert/bfs_dfs/dot/gilbert_dfs_r_"+amout_v+".dot",
        "./graph2/outputs/gilbert/bfs_dfs/img/gilbert_dfs_r_"+amout_v+".png");

        Grafo prim_g = algoritmos_g_g.prim("./graph2/outputs/gilbert/prim_kruskal","GILBERT_"+amout_v);
        prim_g.write("./graph2/outputs/gilbert/prim_kruskal/dot/gilbert_prim_"+amout_v+".dot",prim_g, dirigido);
        prim_g.dibujar("./graph2/outputs/gilbert/prim_kruskal/dot/gilbert_prim_"+amout_v+".dot",
        "./graph2/outputs/gilbert/prim_kruskal/img/gilbert_prim_"+amout_v+".png");

        Grafo kruskal_d_g = algoritmos_g_g.kruskal_d("./graph2/outputs/gilbert/prim_kruskal","GILBERT_"+amout_v);
        kruskal_d_g.write("./graph2/outputs/gilbert/prim_kruskal/dot/gilbert_kruskal_d_"+amout_v+".dot",kruskal_d_g, dirigido);
        kruskal_d_g.dibujar("./graph2/outputs/gilbert/prim_kruskal/dot/gilbert_kruskal_d_"+amout_v+".dot",
        "./graph2/outputs/gilbert/prim_kruskal/img/gilbert_kruskal_d_"+amout_v+".png");

        Grafo kruskal_i_g = algoritmos_g_g.kruskal_i("./graph2/outputs/gilbert/prim_kruskal","GILBERT_"+amout_v);
        kruskal_i_g.write("./graph2/outputs/gilbert/prim_kruskal/dot/gilbert_kruskal_i_"+amout_v+".dot",kruskal_i_g, dirigido);
        kruskal_i_g.dibujar("./graph2/outputs/gilbert/prim_kruskal/dot/gilbert_kruskal_i_"+amout_v+".dot",
        "./graph2/outputs/gilbert/prim_kruskal/img/gilbert_kruskal_i_"+amout_v+".png");

        Grafo dijkstra_g = algoritmos_g_g.dijkstra(g_g.getNodos().get(0), g_g.getNodos().get((int)(Math.random()*(amout_v)-1)), "./graph2/outputs/gilbert/dijkstra","GILBERT_"+amout_v);
        dijkstra_g.write("./graph2/outputs/gilbert/dijkstra/dot/gilbert_dijkstra_"+amout_v+".dot",dijkstra_g, dirigido);
        dijkstra_g.dibujar("./graph2/outputs/gilbert/dijkstra/dot/gilbert_dijkstra_"+amout_v+".dot",
        "./graph2/outputs/gilbert/dijkstra/img/gilbert_dijkstra_"+amout_v+".png");

        // Grafo Geografico
        Grafo g_ge = Grafo.geograficoSimpleGraph(amout_v, 20, 100, 100, dirigido, ciclos);
        Algoritmos algoritmos_g_ge = new Algoritmos(g_ge);
        g_ge.randomEdgeValues(1, 10, g_ge);

        g_ge.write("./graph2/outputs/geografico/dot/geografico_"+amout_v+".dot",g_ge, dirigido);
        g_ge.dibujar("./graph2/outputs/geografico/dot/geografico_"+amout_v+".dot",
        "./graph2/outputs/geografico/img/geografico_"+amout_v+".png");

        Grafo bfs_ge = algoritmos_g_ge.bfs(g_ge.getNodos().get(0));
        bfs_ge.write("./graph2/outputs/geografico/bfs_dfs/dot/geografico_bfs_"+amout_v+".dot",bfs_ge, dirigido);
        bfs_ge.dibujar("./graph2/outputs/geografico/bfs_dfs/dot/geografico_bfs_"+amout_v+".dot",
        "./graph2/outputs/geografico/bfs_dfs/img/geografico_bfs_"+amout_v+".png");

        Grafo dfs_i_ge = algoritmos_g_ge.dfs_i(g_ge.getNodos().get(0));
        dfs_i_ge.write("./graph2/outputs/geografico/bfs_dfs/dot/geografico_dfs_i_"+amout_v+".dot",dfs_i_ge, dirigido);
        dfs_i_ge.dibujar("./graph2/outputs/geografico/bfs_dfs/dot/geografico_dfs_i_"+amout_v+".dot",
        "./graph2/outputs/geografico/bfs_dfs/img/geografico_dfs_i_"+amout_v+".png");

        Grafo dfs_r_ge = algoritmos_g_ge.dfs_r(g_ge.getNodos().get(0));
        dfs_r_ge.write("./graph2/outputs/geografico/bfs_dfs/dot/geografico_dfs_r_"+amout_v+".dot",dfs_r_ge, dirigido);
        dfs_r_ge.dibujar("./graph2/outputs/geografico/bfs_dfs/dot/geografico_dfs_r_"+amout_v+".dot",
        "./graph2/outputs/geografico/bfs_dfs/img/geografico_dfs_r_"+amout_v+".png");

        Grafo prim_ge = algoritmos_g_ge.prim("./graph2/outputs/geografico/prim_kruskal","GEOGRAFICO_"+amout_v);
        prim_ge.write("./graph2/outputs/geografico/prim_kruskal/dot/geografico_prim_"+amout_v+".dot",prim_ge, dirigido);
        prim_ge.dibujar("./graph2/outputs/geografico/prim_kruskal/dot/geografico_prim_"+amout_v+".dot",
        "./graph2/outputs/geografico/prim_kruskal/img/geografico_prim_"+amout_v+".png");

        Grafo kruskal_d_ge = algoritmos_g_ge.kruskal_d("./graph2/outputs/geografico/prim_kruskal","GEOGRAFICO_"+amout_v);
        kruskal_d_ge.write("./graph2/outputs/geografico/prim_kruskal/dot/geografico_kruskal_d_"+amout_v+".dot",kruskal_d_ge, dirigido);
        kruskal_d_ge.dibujar("./graph2/outputs/geografico/prim_kruskal/dot/geografico_kruskal_d_"+amout_v+".dot",
        "./graph2/outputs/geografico/prim_kruskal/img/geografico_kruskal_d_"+amout_v+".png");

        Grafo kruskal_i_ge = algoritmos_g_ge.kruskal_i("./graph2/outputs/geografico/prim_kruskal","GEOGRAFICO_"+amout_v);
        kruskal_i_ge.write("./graph2/outputs/geografico/prim_kruskal/dot/geografico_kruskal_i_"+amout_v+".dot",kruskal_i_ge, dirigido);
        kruskal_i_ge.dibujar("./graph2/outputs/geografico/prim_kruskal/dot/geografico_kruskal_i_"+amout_v+".dot",
        "./graph2/outputs/geografico/prim_kruskal/img/geografico_kruskal_i_"+amout_v+".png");

        Grafo dijkstra_ge = algoritmos_g_ge.dijkstra(g_ge.getNodos().get(0), g_ge.getNodos().get((int)(Math.random()*(amout_v)-1)), "./graph2/outputs/geografico/dijkstra","GEOGRAFICO_"+amout_v);
        dijkstra_ge.write("./graph2/outputs/geografico/dijkstra/dot/geografico_dijkstra_"+amout_v+".dot",dijkstra_ge, dirigido);
        dijkstra_ge.dibujar("./graph2/outputs/geografico/dijkstra/dot/geografico_dijkstra_"+amout_v+".dot",
        "./graph2/outputs/geografico/dijkstra/img/geografico_dijkstra_"+amout_v+".png");

        // Grafo Barabasi
        Grafo g_b = Grafo.barabasiGraph(amout_v, 3, 0.6, dirigido, ciclos);
        Algoritmos algoritmos_b = new Algoritmos(g_b);
        g_b.randomEdgeValues(1, 10, g_b);

        g_b.write("./graph2/outputs/barabasi/dot/barabasi_"+amout_v+".dot",g_b, dirigido);
        g_b.dibujar("./graph2/outputs/barabasi/dot/barabasi_"+amout_v+".dot",
        "./graph2/outputs/barabasi/img/barabasi_"+amout_v+".png");

        Grafo bfs_b = algoritmos_b.bfs(g_b.getNodos().get(0));
        bfs_b.write("./graph2/outputs/barabasi/bfs_dfs/dot/barabasi_bfs_"+amout_v+".dot",bfs_b, dirigido);
        bfs_b.dibujar("./graph2/outputs/barabasi/bfs_dfs/dot/barabasi_bfs_"+amout_v+".dot",
        "./graph2/outputs/barabasi/bfs_dfs/img/barabasi_bfs_"+amout_v+".png");

        Grafo dfs_i_b = algoritmos_b.dfs_i(g_b.getNodos().get(0));
        dfs_i_b.write("./graph2/outputs/barabasi/bfs_dfs/dot/barabasi_dfs_i_"+amout_v+".dot",dfs_i_b, dirigido);
        dfs_i_b.dibujar("./graph2/outputs/barabasi/bfs_dfs/dot/barabasi_dfs_i_"+amout_v+".dot",
        "./graph2/outputs/barabasi/bfs_dfs/img/barabasi_dfs_i_"+amout_v+".png");

        Grafo dfs_r_b = algoritmos_b.dfs_r(g_b.getNodos().get(0));
        dfs_r_b.write("./graph2/outputs/barabasi/bfs_dfs/dot/barabasi_dfs_r_"+amout_v+".dot",dfs_r_b, dirigido);
        dfs_r_b.dibujar("./graph2/outputs/barabasi/bfs_dfs/dot/barabasi_dfs_r_"+amout_v+".dot",
        "./graph2/outputs/barabasi/bfs_dfs/img/barabasi_dfs_r_"+amout_v+".png");

        Grafo prim_b = algoritmos_b.prim("./graph2/outputs/barabasi/prim_kruskal","BARABASI_"+amout_v);
        prim_b.write("./graph2/outputs/barabasi/prim_kruskal/dot/barabasi_prim_"+amout_v+".dot",prim_b, dirigido);
        prim_b.dibujar("./graph2/outputs/barabasi/prim_kruskal/dot/barabasi_prim_"+amout_v+".dot",
        "./graph2/outputs/barabasi/prim_kruskal/img/barabasi_prim_"+amout_v+".png");

        Grafo kruskal_d_b = algoritmos_b.kruskal_d("./graph2/outputs/barabasi/prim_kruskal","BARABASI_"+amout_v);
        kruskal_d_b.write("./graph2/outputs/barabasi/prim_kruskal/dot/barabasi_kruskal_d_"+amout_v+".dot",kruskal_d_b, dirigido);
        kruskal_d_b.dibujar("./graph2/outputs/barabasi/prim_kruskal/dot/barabasi_kruskal_d_"+amout_v+".dot",
        "./graph2/outputs/barabasi/prim_kruskal/img/barabasi_kruskal_d_"+amout_v+".png");

        Grafo kruskal_i_b = algoritmos_b.kruskal_i("./graph2/outputs/barabasi/prim_kruskal","BARABASI_"+amout_v);
        kruskal_i_b.write("./graph2/outputs/barabasi/prim_kruskal/dot/barabasi_kruskal_i_"+amout_v+".dot",kruskal_i_b, dirigido);
        kruskal_i_b.dibujar("./graph2/outputs/barabasi/prim_kruskal/dot/barabasi_kruskal_i_"+amout_v+".dot",
        "./graph2/outputs/barabasi/prim_kruskal/img/barabasi_kruskal_i_"+amout_v+".png");

        Grafo dijkstra_b = algoritmos_b.dijkstra(g_b.getNodos().get(0), g_b.getNodos().get((int)(Math.random()*(amout_v)-1)), "./graph2/outputs/barabasi/dijkstra","BARABASI_"+amout_v);
        dijkstra_b.write("./graph2/outputs/barabasi/dijkstra/dot/barabasi_dijkstra_"+amout_v+".dot",dijkstra_b, dirigido);
        dijkstra_b.dibujar("./graph2/outputs/barabasi/dijkstra/dot/barabasi_dijkstra_"+amout_v+".dot",
        "./graph2/outputs/barabasi/dijkstra/img/barabasi_dijkstra_"+amout_v+".png");

    }

}