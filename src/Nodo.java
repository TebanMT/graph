
/**
 * Class Nodo
 *
 * Contiene las propiedades de un nodo e implementa la interfaz Comparable,
 * para que se pueda comaprar y ordenar por peso.
*/
public class Nodo implements Comparable<Nodo> {
    private int id;
    private float peso; // un atributo que se utiliza para el algoritmo de dijstra
    private String label;
    private int grado; // Grado del nodo

    public Nodo(int id, float peso, String label, int grado) {
        this.id = id;
        this.peso = peso;
        this.label = label;
        this.grado = grado;
    }

    public Nodo(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getGrado() {
        return grado;
    }

    public void setGrado(int grado) {
        this.grado = grado;
    }

    @Override
    public int compareTo(Nodo e) {
        return Float.compare( this.getPeso() , e.getPeso());
    }

}