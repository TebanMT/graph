import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Class Grafo
 *
 * La clase Grafo contiene las propiedades de un grafo (aristas, nodos y el tipo)
 *
 * Las aristas son representadas mediante un HashMap de la clase Arista y un identificador
 * entero consecutivo.
 * Los vertices son representados mediante un HashMap de la clase Nodo y un identificador
 * entero consecutivo.
 * El tipo de grafo es representado con un booleano: true para grafo dirigido, false para
 * un grafo no dirigido.
 * NOTA: el identificador entero de los HashMap es el mismo que el identificador del objeto
 * correspondiente.
 *
 * Tambien contiene los metodos estaticos para la creacion de cierto tipo de grafos
 * (erdos, gilber, geografico y barabasi) y los metodos publicos que realizan acciones
 * sobre cualquier grafo.
 * Cabe mencionar que un grafo dirigido es aquel que puede tener las aristas
 * (0,1) y (1,0), estó en un grafo no dirigido no se permite, ya que (0,1) = (1,0).
 * Está propiedad se ve reflejada al crear algun grafo y controlada por los metodos:
 * getAristas, getPeso y areConnected. Ya que la manera en que realizan acciones cambia
 * de acuerdo al tipo de grafo.
*/

public class Grafo {

    private HashMap<Integer, Arista> aristas = new HashMap<Integer, Arista>(); // HashMap de la clase Aristas
    private HashMap<Integer, Nodo> nodos = new HashMap<Integer, Nodo>();// HashMap de la clase Nodos
    private boolean dirigido; // Dirigido (true) No Dirigido (false)

    public HashMap<Integer, Arista> getAristas() {
        return aristas;
    }

    /**
     * Sobreescritura del metodo getAristas. Busca los nodos adyacentes a el nodo 'n'.
     * Si el grafo es dirigido, toma como arista adyacente tanto a la arista (n,w) como a la (w,n),
     * dicho de otra manera, no importa si el nodo n está en el origen o destino de la arista.
     * De lo contrario, si el grafo es dirigido, toma como arista adyacente solo a la arista (n, w),
     * en otras palabras, solamente cuando el nodo n está en el origen de la arista.
     *
     * @param n Nodo
     * @return Lista de aristas adyacentes al nodo 'n'
     */
    public List<Arista> getAristas(Nodo n) {
        List<Arista> ar = new ArrayList<Arista>();
        for (Arista arista : this.aristas.values()) {
            if ( (arista.getOrigen() == n || arista.getDestino() == n) && !this.dirigido){
                ar.add(arista);
            }
            if (arista.getOrigen() == n && this.dirigido){
                ar.add(arista);
            }
        }
        return ar;
    }

    public void setAristas(HashMap<Integer, Arista> aristas) {
        this.aristas = aristas;
    }

    public HashMap<Integer, Nodo> getNodos() {
        return nodos;
    }

    public void setNodos(HashMap<Integer, Nodo> nodos) {
        this.nodos = nodos;
    }

    public boolean isDirigido() {
        return dirigido;
    }

    public void setDirigido(boolean dirigido) {
        this.dirigido = dirigido;
    }

    public void addNodo(Integer key, Nodo n) {
        this.nodos.put(key, n);
    }

    public void addArista(Integer key, Arista a) {
        this.aristas.put(key, a);
    }

    /**
     * Elimina la arista 'a' del grafo
     * @param a Arista a eliminar
     */
    public void deleteA(Arista a){
        this.aristas.entrySet()
            .removeIf(
                entry -> (a.equals(entry.getValue())));
    }

    /**
     * Obtiene el peso de la arista que contiene a los nodos origen y destino.
     * Si el grafo es NO DIRIGIDO, no importa el orden en que encuentren;
     * estó quiere decir que, tanto la arista (origen, destino) y (destino, origen)
     * son validas y se optendra su peso.
     * Si el grafo es DIRIGIDO, el orden sí importa, la única arista valida es (origen, destino).
     *
     * @param origen
     * @param destino
     * @return
     */
    public float getPeso(Nodo origen, Nodo destino){
        float res[] = new float[1];
        this.getAristas().forEach((key,tab)->{
            if (((tab.getOrigen()==destino && tab.getDestino()==origen)
                || (tab.getOrigen()==origen && tab.getDestino()==destino)) && !this.dirigido)
                    res[0] = tab.getPeso();
            if ((tab.getOrigen() == origen && tab.getDestino()==destino) && this.dirigido)
                res[0] = tab.getPeso();
        });
        return res[0];
    }

    /**
     * Crear n vértices y elegir uniformemente al azar
     * m distintos pares de distintos vértices
     *
     * @param n Numero de vertices (nodos)
     * @param m 'm' distintos pares de distintos vertices
     * @param dirigido Boolean para grafo dirigido o no dirigido
     * @param self Boolean para auto conexiones de un Nodo
     * @return Grafo Erdos
     */
    public static Grafo erdosGraph(int n, int m, boolean dirigido, boolean self) {
        Grafo grafo = new Grafo();
        grafo.setDirigido(dirigido);
        grafo.generarNodos(n, grafo); // se generan los nodos
        int key = 0; // key para cada arista, contador
        for (int i = 0; i < m; i++) {
            int n1 = (int)(Math.random()*(n)); //numero aleatorio para el origen
            int n2 = (int)(Math.random()*(n)); //numero aleatorio para el destino
            if (n1 == n2 && !self ) {i--;continue;} // si n1=n2 y no contine aristas a sí mismo elige otro par de numeros random
            Nodo origen = grafo.getNodos().get(n1); //Nodo origen
            Nodo destino = grafo.getNodos().get(n2); //Nodo destino
            if (!grafo.areConnected(origen, destino, grafo, dirigido)) {
                //No estan conectados, entonces los conecto
                Arista arista = new Arista(key, origen, destino, 0);
                grafo.addArista(key, arista);
                key += 1;
            }
        }
        return grafo;
    }

    /**
     * Crea m aristas crea n vertices y poner una arista entre cada par independiente
     * y uniformemente con probabilidad p
     *
     * @param n Número de nodos
     * @param m Número de aristas
     * @param p Probabilidad que se conecten cada par de vertices
     * @param dirigido Boolean para grafo dirigido o no dirigido
     * @param self Boolean para auto conexiones de un Nodo
     * @return Grafo gilbert
     */
    public static Grafo gilbertGraph(int n, int m, double p, boolean dirigido, boolean self) {
        Grafo grafo = new Grafo();
        grafo.generarNodos(n, grafo); // se generan los nodos
        grafo.setDirigido(dirigido);
        int key = 0; // key para cada arista, contador
        for (int i = 0; i < m; i++) {
            double p_aux = Math.random(); // probabilidad auxiliar en cada ciclo
            int n1 = (int)(Math.random()*(n)); //numero aleatorio para el origen
            int n2 = (int)(Math.random()*(n)); //numero aleatorio para el destino
            if (n1 == n2 && !self ) {i--;continue;}// si n1=n2 y no contine aristas a sí mismo elige otro par de numeros random
            Nodo origen = grafo.getNodos().get(n1); //Nodo aleatorio origen
            Nodo destino = grafo.getNodos().get(n2); //Nodo aleatorio destino
            if ((!grafo.areConnected(origen, destino, grafo, dirigido)) && p >= p_aux ) {
                //No estan conectados y la probalidad dada es mayor o igual a la probabilidad auxiliar
                Arista arista = new Arista(key, origen, destino, 0);
                grafo.addArista(key, arista);
                key += 1;
            }
        }
        return grafo;
    }

    /**
     * Coloca n vértices en un rectangulo unitario con coordenadas uniformes (o normales)
     * y coloca una arista entre cada par que queda en distancia r o menor.
     *
     * @param n Numero de vértices
     * @param r Distancia maxima para que un par de vertices se pueda conectar
     * @param x Longitud del eje x
     * @param y Longitud del eje y
     * @param dirigido Boolean para grafo dirigido o no dirigido
     * @param self Boolean para auto conexiones de un Nodo
     * @return Grafo Geografico Simple
     */
    public static Grafo geograficoSimpleGraph(int n, double r, int x, int y, boolean dirigido, boolean self) {
        Grafo grafo = new Grafo();
        grafo.generarNodos(n, grafo);
        grafo.setDirigido(dirigido);
        Integer key = 0;
        for (Nodo origen: grafo.getNodos().values()) {
            double i_x = Math.random()*(x-0)+0; // eje x del nodo origen
            double i_y = Math.random()*(y-0)+0; // eje y del nodo origen
            for(Nodo destino: grafo.getNodos().values()){
                if (origen.getLabel() == destino.getLabel() && !self ) {continue;} // si no contine aristas a sí mismo continua
                double j_x = Math.random()*(x-0)+0; // eje x del nodo destino
                double j_y = Math.random()*(y-0)+0;; // eje y del nodo destino
                if (grafo.areClose(i_x, i_y, j_x, j_y, r) && !grafo.areConnected(origen, destino, grafo, dirigido)) {
                    // si estan cerca y no estan conectados, se conectan
                    Arista arista = new Arista(key, origen, destino, 0);
                    grafo.addArista(key, arista);
                    key += 1;
                }
            }
        }
        return grafo;
    }

    /**
     * Coloca n vértices uno por uno,
     * asignando a cada uno d aristas a vértices distintos de tal manera que la
     * probabilidad de que el vértice nuevo se conecte a un vértice existente v es
     * proporcional a la cantidad de aristas que v tiene actualmente - los primeros d
     * vértices se conecta todos a todos.
     *
     * @param n Número de vertices
     * @param d Número de aristas
     * @param p Probabilidad de referencia para que se conencten
     * @param dirigido Boolean para grafo dirigido o no dirigido
     * @param self Boolean para auto conexiones de un Nodo
     * @return Grafo Barabasi
     */
    public static Grafo barabasiGraph(int n, int d, double p, boolean dirigido, boolean self) {
        Grafo grafo = new Grafo();
        grafo.generarNodos(2, grafo);
        grafo.setDirigido(dirigido);
        grafo.initBarabasi(grafo); // Se inicia el grafo
        int key = 1;
        for (int i = 2; i < n; i++) { // se crean nodos a partir del numero 2
            // Para cada nuevo nodo se crea una lista con los nodos que van hasta el momento
            // De está lista se sacan los nodos, de manera aleatoria, a los que se podra conectar
            // una nueva arista.
            List<Integer> nodos = new ArrayList<Integer>();
            for (int k = 0; k < i+1; k++) {
                nodos.add(k);
            }
            Nodo origen = new Nodo(i, 0, "Q"+i, 0);
            grafo.addNodo(i, origen);
            for(int j = 0; j < d; j++){ // d aristas para cada nodo
                if (nodos.size()==0) // si ya no hay más nodos a donde conectarse, continua hata que se termine el ciclo
                    continue;
                //Se elije un nodo aleatorio de la lista de nodos actuales
                int n_random = (int)(Math.random()*(nodos.size())-1);
                int id_destino = nodos.get(n_random);
                Nodo destino = grafo.getNodos().get(id_destino);
                //
                int grado_destino = destino.getGrado(); // Grado del nodo destino
                double grado_total = (grafo.getAristas().size()*2); // grado total del grafo
                double prob = (grado_destino/grado_total); // probabilidad de que los nodos origen y destino sean conecatdos
                if (!self && origen == destino) {j--;nodos.remove(n_random);continue;}
                if (p >= prob && !grafo.areConnected(origen, destino, grafo, dirigido)){
                    //La probabilidad de referencia es mayor a la probabilidad de conexion y no estan conectados
                    Arista arista = new Arista(key, origen, destino, 0);
                    grafo.addArista(key, arista);
                    int new_grado_origen = origen.getGrado() + 1;
                    origen.setGrado(new_grado_origen);
                    grado_destino = grado_destino + 1; // New grado destino
                    destino.setGrado(grado_destino);
                    key += 1;
                }
                //Al final del ciclo se remueve el nodo para que no pueda volver a ser elejido
                //en el siguiente ciclo.
                nodos.remove(n_random);
            }
        }
        return grafo;
    }

    /**
     * Se inicializa in grafo barabasi conectando el nodo 0 y 1
     * @param g Grafo
     */
    private void initBarabasi(Grafo g) {
        Arista arista1 = new Arista(0, g.getNodos().get(0), g.getNodos().get(1), 0);
        g.addArista(0, arista1);
        g.getNodos().get(0).setGrado(1);
        g.getNodos().get(1).setGrado(1);
    }

    /**
     * Funcion que indentifica, dado las cordenadas x,y de 2 nodos,
     * si estos, de acuerdo a sus coordenadas en un plano, estan igual
     * o más cerca que la distancia r. Donde r es una distancia en numeros reales positivos.
     *
     * @param i_x cordenada x del nodo origen
     * @param i_y cordenada y del nodo origen
     * @param j_x cordenada x del nodo destino
     * @param j_y cordenada y del nodo detino
     * @param r Distancia minima para que los nodos sean conciderados cerca
     * @return false si no esan lo suficientemente cerca, true si sí estan cerca
     */
    private Boolean areClose(double i_x, double i_y, double j_x, double j_y, double r){
        Double x_coord_square = Math.pow((j_x-i_x), 2);
        Double y_coord_square = Math.pow((j_y-i_y), 2);
        Double d = Math.sqrt((x_coord_square + y_coord_square)); //Distancia, usando la formula de distancia entre dos puntos
        if (r >= d) {
            // la distancia r es mayor o igual que la distancia entre los dos puntos,
            // por lo tanto si estan cerca uno del otro
            return true;
        }else{ return false; }
    }

    /**
     * Funcion que identifica si un par de nodos estan conectados entre si en un grafo
     * @param n1 Nodo origen
     * @param n2 Nodo destino
     * @param g Grafo en el que sera evaluado
     * @param dirigido Boolean para grafo dirigido o no dirigido
     * @return false si no estan conecatdos, true si sí lo estan.
     */
    private Boolean areConnected(Nodo n1, Nodo n2, Grafo g, Boolean dirigido) {
        Boolean result = false;
        HashMap<Integer, Arista> hash_aristas = g.getAristas();
        for (int i = 0; i < hash_aristas.size(); i++) {
            Arista arista_i = hash_aristas.get(i);
            if ((n1 == arista_i.getOrigen() && n2 == arista_i.getDestino()) && dirigido){
                // Dirigido
                result = true;
              break;
            } 
            if ((n1 == arista_i.getOrigen() || n1 == arista_i.getDestino())
                && (n2 == arista_i.getDestino() || n2 == arista_i.getOrigen()) && !dirigido) {
                // No dirigido
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Funcion auxiliar para generar n cantidad de nodos
     *
     * @param n Numero de Nodos a generar
     * @param g Grafo en el cual se van a generar los nodos
     */
    private void generarNodos(int n, Grafo g) {
        for (int i = 0; i < n; i++) {
            Nodo nodo = new Nodo(i, 0, "Q"+i, 0);
            g.addNodo(i, nodo);
        }
    }

    /**
     * RandomEdgeValues asigna valores aleatorios a
     * cada arista dentro de un rango.
     *
     * @param min valor minimo del rango
     * @param max valor maximo del rango
     * @param g grafo
     */
    public void randomEdgeValues(float min, float max, Grafo g){
        for (Arista a : getAristas().values()) {
            float valorAleatorio = (float) Math.random()*(max-min)+min; // Valor entre min y max, ambos incluidos.
            a.setPeso(valorAleatorio);
        }
    }

    /**
     * Escribe y guarda un archivo .dot con la descripcion y elementos del grafo.
     *
     * @param direccionDot Dirección donde se guardara el archivo
     * @param g grafo
     * @param dirigido Boolean para grafo dirigido o no dirigido
     */
    public void write(String direccionDot, Grafo g, Boolean dirigido) {
        /**TODO: Se realizan 2 for, uno para "pintar" las aristas junto con sus nodos involucrados
           pero este proceso no "pinta" los nodos que no estan conecatado, por lo cual se realiza otro
           ciclo para "pintar" los nodos que no estan en el conjunto de las aristas. Este proceso es, en
           mayor o menor medida, ineficiente y se ve feo jeje. Debe haber una mejor forma de hacerlo.
        */
        try(BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(direccionDot))) ){
            if (dirigido) {out.write("digraph G {");}
            else{out.write("graph G {");}
            out.write("node [shape=circle];\nnode [style=filled];\nnode [fillcolor=\"#EEEEEE\"];\nnode [color=\"#EEEEEE\"];\nedge [color=\"#31CEF0\"];");
            out.newLine();
            HashSet<String> nodos_connected = new HashSet<String>();
            for(Arista e: g.getAristas().values()){
                if (dirigido) {
                    out.write(e.getOrigen().getLabel()+" -> "+e.getDestino().getLabel()+"[label="+e.getPeso()+"];");
                }
                else{
                        out.write(e.getOrigen().getLabel()+" -- "+e.getDestino().getLabel()+"[label="+e.getPeso()+", color="+e.getColor()+"];");
                }
                out.newLine();
                nodos_connected.add(e.getOrigen().getLabel());
                nodos_connected.add(e.getDestino().getLabel());
            }
            for (Nodo n : g.getNodos().values()) {
                if (!nodos_connected.contains(n.getLabel())) {
                    out.write(n.getLabel()+";");
                    out.newLine();
                }
            }
            out.write("}");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea una imangen .png del grafo, a partir de un archivo .dot
     * @param direccionDot Dirección del archivo .dot
     * @param direccionPng Dirección donde se guardara la imagen .png
     */
    public void dibujar(String direccionDot, String direccionPng ){
        try
        {
            ProcessBuilder pbuilder;
            /*
            * Realiza la construccion del comando
            * en la linea de comandos esto es:
            * dot -Tpng -o archivo.png archivo.dot
            */
            pbuilder = new ProcessBuilder( "dot", "-Tpng", "-o", direccionPng, direccionDot );
            pbuilder.redirectErrorStream( true );
            //Ejecuta el proceso
            pbuilder.start();
        } catch (Exception e) {e.printStackTrace(); }
    }

}